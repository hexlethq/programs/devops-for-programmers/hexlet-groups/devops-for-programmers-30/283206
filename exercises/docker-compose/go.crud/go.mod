module github.com/nightlord189/go.crud

go 1.13

require (
	github.com/gin-gonic/gin v1.7.2
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12
)
