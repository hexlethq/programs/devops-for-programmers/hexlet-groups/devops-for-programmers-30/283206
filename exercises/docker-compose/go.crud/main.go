package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/nightlord189/go.crud/handler"
)

func main() {
	config := loadConfig("config.json")
	port := config["httpPort"]

	handler.InitDb(
		config["dbHost"].(string),
		int((config["dbPort"]).(float64)),
		config["dbUser"].(string),
		config["dbPassword"].(string),
		config["dbName"].(string))

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.String(200, "success "+time.Now().String())
	})
	r.GET("/note/:id", handler.ReadNote)
	r.DELETE("/note/:id", handler.DeleteNote)
	r.PUT("/note", handler.CreateNote)
	r.Run(fmt.Sprintf(":%v", port))
}

func loadConfig(filename string) map[string]interface{} {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var data map[string]interface{}
	if err := json.NewDecoder(file).Decode(&data); err != nil {
		log.Fatal(err)
	}
	return data
}
