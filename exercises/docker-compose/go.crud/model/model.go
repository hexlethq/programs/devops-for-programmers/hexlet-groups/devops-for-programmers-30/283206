package model

import "time"

type Note struct {
	ID        int    `json:"id" gorm:"column:id;primaryKey;"`
	Text      string `json:"text" gorm:"text"`
	CreatedAt time.Time
}

func (n Note) TableName() string {
	return "notes"
}
