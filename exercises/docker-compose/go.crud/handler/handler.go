package handler

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/nightlord189/go.crud/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var dbInstance *gorm.DB

func InitDb(host string, port int, user, password, dbName string) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable", host, user, password, dbName, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	dbInstance = db
	dbInstance.AutoMigrate(model.Note{})
}

func DeleteNote(c *gin.Context) {
	id := c.Param("id")

	err := dbInstance.Where("id = ?", id).Delete(&model.Note{}).Error

	if err != nil {
		c.String(422, "err: "+err.Error())
		return
	}
	c.JSON(200, "success")
}

func ReadNote(c *gin.Context) {
	id := c.Param("id")

	var result model.Note
	err := dbInstance.First(&result, "id = ?", id).Error

	if err != nil {
		c.String(422, "err: "+err.Error())
		return
	}
	c.JSON(200, result)
}

func CreateNote(c *gin.Context) {
	var result model.Note

	err := c.ShouldBindJSON(&result)
	if err != nil {
		c.String(400, "err: "+err.Error())
		return
	}

	err = dbInstance.Create(&result).Error

	if err != nil {
		c.String(422, "err: "+err.Error())
		return
	}
	c.JSON(200, result)
}
