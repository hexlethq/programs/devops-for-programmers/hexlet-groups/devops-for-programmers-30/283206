terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "1.22.2"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

// Токен DO и путь к приватному ключу, будут передаваться через CLI
variable "do_token" {}

data "digitalocean_ssh_key" "main" {
  name = "aburavov"
}

data "digitalocean_ssh_key" "ci" {
  name = "ci"
}

// Создаём дроплет
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet
resource "digitalocean_droplet" "web1" {
  image  = "ubuntu-20-10-x64"
  name   = "ansible-for-servers-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.main.id,
    data.digitalocean_ssh_key.ci.id
  ]
}

//второй дроплет
resource "digitalocean_droplet" "web2" {
  image  = "ubuntu-20-10-x64"
  name   = "ansible-for-servers-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [
    data.digitalocean_ssh_key.main.id,
    data.digitalocean_ssh_key.ci.id
  ]
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "balancer01" {
  name   = "balancer-ansible-for-servers-01"
  region = "ams3"

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port     = 80
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port     = 5000
    target_protocol = "http"
  }

  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет живой и принимает запросы
  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web1.id,
    digitalocean_droplet.web2.id
  ]
}

//А-запись для поддомена задания
resource "digitalocean_record" "terraform" {
  domain = "hexpro.xyz"
  type   = "A"
  name   = "ansible-for-servers"
  value  = digitalocean_loadbalancer.balancer01.ip
}

// Outputs похожи на возвращаемые значения. Они позволяют сгруппировать информацию или распечатать то, что нам необходимо
// https://www.terraform.io/docs/language/values/outputs.html
output "droplets" {
  // Обращение к ресурсу. Каждый ресурс имеет атрибуты, ккоторым можно получить доступ
  value = [
    digitalocean_droplet.web1,
    digitalocean_droplet.web2
  ]
}