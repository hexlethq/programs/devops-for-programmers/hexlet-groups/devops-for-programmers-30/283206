package main

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

var port = 3000

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.String(200, "success "+time.Now().String())
	})
	r.Run(fmt.Sprintf(":%d", port))
}
